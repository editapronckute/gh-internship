<div id='main-wrapper'>
 <div id=background-street>
   <img class="garbage" src="img/Afval@2x.png" alt="trash">
   <img class="air" src="img/Achtergrond_goed@2x.png" alt="clean-air">
   <img class="birds" src="img/vogels@2x.png" alt="birds">
   <img class="boom" src="img/bomen@2x.png" alt="boom">
   <img class="boom-bad" src="img/bomen_slecht@2x.png" alt="boom">
   
 </div>

 <div id= "wrapper-begin">

    <div id='titleScreen'>
      <h1 class='maintitle'>Welcome to Michelle's world</h1>
      <p class='xplane'>Use the mouse to drag the outfit of your choosing to Michelle.</p>
    </div> <div class='clear'></div>
    
    <span><div id='button-begin'><p id='button-begin-text'></p></div></span>

   <audio id="city">
    <source src="audio/street-bit-louder-noise.mp3" type="audio/mpeg">
    </audio>
    <audio id="intro"><source src="audio/introduction_occasion.mp3" type="audio/mpeg"></audio>
  
    
  </div> <!-- einde wrapper-begin -->
 
  <div class="intro-michelle-face-ani"><img class="michelle-face-img" src="img/Michelle_DDWV3.gif" alt="michelle"></div>
  <div id='michelle_intro-face'><img class="michelle-face-img" src="img/michelle-still@2x.png" alt="michelle"></div>
  

   <div id='wrapper-welcom'> <!--begin eeste module-->
    <div class='drop-area' id='drop1'></div>
    <div class="michelle-face"><img class="michelle-face-img" src="img/Michelle_DDWV3.gif" alt="michelle"></div>
    <div class='authority keuze1 ui-widget-content sustainable' id=autority-keuze-s><!-- begin discription --><div class='disciption-dress' id='autority-keuze-s-verhaal'><p>The dress is made from recycled materials sourced from Europe. The employees belong to a labour union</p></div><!-- end-disciption--></div> 
    <div class='authority keuze1 ui-widget-content tweede-hands' id=autority-keuze-t><!-- begin discription --><div class='disciption-dress' id='autority-keuze-t-verhaal'><p>This dress was custom made for a costume party in the early 2000s. Might not be the fashion statement of today but it still has plenty of life left in it!</p></div><!-- end-disciption--></div>
    <div class='authority keuze1 ui-widget-content fast-fashion' id=autority-keuze-f><!-- begin discription --><div class='disciption-dress' id='autority-keuze-f-verhaal'><p>The underskirt has various layers of different fabrics and the top red polyester layer is very soft to the touch. It’s the newest highlight of the season!</p></div><!-- end-disciption--></div>
    <audio id="gala"><source src="audio/auorithyGalla.mp3" type="audio/mpeg"></audio>
 </div> <!--einde eeste module-->

 <audio id="thanks"><source src="audio/Thanks.wav" type="audio/mpeg"></audio>
 <audio id="scarcity"><source src="audio/scarcity.wav" type="audio/mpeg"></audio>
 <audio id="scarcity-mist"><source src="audio/Scarcity_mist.mp3" type="audio/mpeg"></audio>
 <audio id="scarcity-thanks"><source src="audio/Scarcity_thanks.wav" type="audio/mpeg"></audio>
 <audio id="date"><source src="audio/date.wav" type="audio/mpeg"></audio>
 <audio id="date-end"><source src="audio/date_end.wav" type="audio/mpeg"></audio> 
 <audio id="resolutions"><source src="audio/resolutions.wav" type="audio/mpeg"></audio>
 <audio id="resolutions"><source src="audio/resolutions.wav" type="audio/mpeg"></audio>
 <audio id="birds-audio"><source src="audio/bird-less-correct.mp3" type="audio/mpeg"></audio>
 <!-- next button --> <div class='next-event next-event-ook'><p id='next-event-text'>NEXT</p></div><!-- next button -->

 <div id='wrapper-scenario2'> <!--begin tweede modual-->
 <div class="countdown"></div>
    <div class='drop-area' id='drop2'></div>
    <div class="michelle-face"><img class="michelle-face-img" src="img/Michelle_DDWV3.gif" alt="michelle"></div>
    <div class='scarcity keuze1 ui-widget-content sustainable' id=scarcity-keuze-s><!-- begin discription --><div class='disciption-dress' id='scarcity-keuze-s-verhaal'><p>A breathable linen dress not bleached, coloured using plant dye. The ruffles mask the wrinkly nature of linen making it easy to preserve.</p></div><!-- end-disciption--></div>
    <div class='scarcity keuze1 ui-widget-content tweede-hands' id=scarcity-keuze-t><!-- begin discription --><div class='disciption-dress' id='scarcity-keuze-t-verhaal'><p>This vintage dress already had one owner and now is ready to find a new one. It is very light and made out of 100% cotton.</p></div><!-- end-disciption--></div>
    <div class='scarcity keuze1 ui-widget-content fast-fashion' id=scarcity-keuze-f><!-- begin discription --><div class='disciption-dress' id='scarcity-keuze-f-verhaal'><p>Sparkle the night away with this limited time offer!</p></div><!-- end-disciption--></div>
 </div> <!--einde tweede modual-->

 <div id='wrapper-scenario3'> <!--begin derde modual-->
    <div class='drop-area' id='drop3'></div>
    <div class="michelle-face"><img class="michelle-face-img" src="img/Michelle_DDWV3.gif" alt="michelle"></div>
    <div class='social keuze1 ui-widget-content sustainable' id=social-keuze-s><!-- begin discription --><div class='disciption-dress' id='social-keuze-s-verhaal'><p>Made from cupro, a byproduct of the cotton industry that usually is wasted. Feels like silk on your skin!</p></div><!-- end-disciption--></div>
    <div class='social keuze1 ui-widget-content tweede-hands' id=social-keuze-t><!-- begin discription --><div class='disciption-dress' id='social-keuze-t-verhaal'><p>A lovely vintage yellow dress. Bring back the 50’s!</p></div><!-- end-disciption--></div>
    <div class='social keuze1 ui-widget-content fast-fashion' id=social-keuze-f><!-- begin discription --><div class='disciption-dress' id='social-keuze-f-verhaal'><p>The dress that is hot this season! Made from 80% cotton and 20% polyester.</p></div><!-- end-disciption--></div>
 </div> <!--einde derde modual-->

 <div id='wrapper-send-off'> <!--begin derde modual-->
        <div id='thanks-for-playing'> 
          <h1 class='send-off-title'>Thanks for playing</h1><dr>
          <p class='text-send-off'>
          Your choices affect the environment!<br><br>  Make sure to check out Michelle's Resolutions before you leave. You can use the QR-code below to post your own commitment:
          </p>
          <div id=qr-code><img src="img/qr_resolutions@2x.png" alt="qr-code"></div>
          <div class='reloading reloading-ook'><p class='play-again'>PLAY AGAIN</p></div>
        </div>
    
    <div class="michelle-face"><img class="michelle-face-img" src="img/Michelle_DDWV3.gif" alt="michelle"></div>
   <!-- <div id='textfield-data'><textarea id="consolelog-data" name="console-log-choice-data" ></textarea></div> -->
 </div> <!--einde derde modual-->

</div><!-- einde main wrapper-->