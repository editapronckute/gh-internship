// importing crypto js library for hashing
const SHA256 = require('crypto-js/sha256');

class Block{
    constructor(index, timestamp, data, previousHash = ''){
        this.index = index; // optional, number of the block within the chain
        this.timestamp = timestamp;//when block was created
        this.data = data; // any data, can be transaction details i.e. amount transferred, sender and/or receiver
        this.previousHash = previousHash; //to ensure the integrity of the chain 
        this.hash = this.calculateHash(); 
        this.nonce = 0; // random number that can be edited and has nothing to do with the block
    }

    // taking the properties and turning them into hash to identify blocks within chains
    calculateHash(){
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce).toString();
    }

    //Proof of work - is used to slow down (require more computing power) creation of new blocks (mining) i.e. by adding 000.. in front of the hash of each new block
    // this slowing down is called the difficulty with computers becoming more powerfull the difficulty is also increasing
    mineBlock(difficulty){
        while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")){
            this.nonce++; // increment as long as the hash doesn't start with 0
            this.hash = this.calculateHash();
        }
        console.log("Block mined: " + this.hash)
    }
}
 
class Blockchain{
    // Genesis Block - the very first block (array) within the chain - must be created manually
    constructor(){
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 5; // the higher the dificulty the longer it takes to mine blocks
    }

    createGenesisBlock(){
        return new Block(0, "01/01/2021", "Genesis block", "0");
    }

    //last block of the chain
    getLatestBlock(){
        return this.chain[this.chain.length - 1];
    }
    // adding new blocks to the chain (needs to keep track of previous block's hash and get a new one too)
    addBlock(newBlock){
        newBlock.previousHash = this.getLatestBlock().hash;
        newBlock.mineBlock(this.difficulty);
        this.chain.push(newBlock);
    }

    //verifying if the chain is valid: all the data is intact and hashes are unchanged 
    isChainValid(){
        for(let i = 1; i< this.chain.length; i++){
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if(currentBlock.hash !== currentBlock.calculateHash()){
                return false;
            }
            if(currentBlock.previousHash !== previousBlock.hash){
                return false
            }
        }
        return true;
    }
}

//creating 2 more block to play with them
let bunnycoin = new Blockchain();

console.log('Mining Block 1...');
bunnycoin.addBlock(new Block(1, "28/01/2021", {amount: 4 }));
console.log('Mining Block 2...');
bunnycoin.addBlock(new Block(2, "30/01/2021", {amount: 8 }));



//Checking if blockchain is valid
// console.log('Is blockchain valid?' + bunnycoin.isChainValid());

//trying to tamper with the data and changing hash to check if validation works as intendended
// bunnycoin.chain[1].data = { amount: 1000 };
// bunnycoin.chain[1].hash = bunnycoin.chain[1].calculateHash();

// console.log('Is blockchain valid?' + bunnycoin.isChainValid());

//checking what the chain/blocks look like
//console.log(JSON.stringify(bunnycoin, null, 4))